//
//  AuthorsService.swift
//  MicroBlog
//
//  Created by Ravikumar Chintakayala on 28/06/2020.
//  Copyright © 2020 Personal. All rights reserved.
//

import Foundation

struct AuthorsService {
    
    let apiClient: APIClient

    init(apiClient: APIClient = .shared ) {
        self.apiClient = apiClient
    }
    
    func getAuthors(pageNumber: Int = 0, pageLimit: Int = 20, completionHandler: @escaping ([Author]?, Error?) -> Void) {
        let url = Endpoint.authors(pageNumber: pageNumber, pageLimit: pageLimit).url
        
        apiClient.get(url, completionHandler: completionHandler)
    }
    
}
