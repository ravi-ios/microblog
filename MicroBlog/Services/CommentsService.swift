//
//  CommentsService.swift
//  MicroBlog
//
//  Created by Ravikumar Chintakayala on 28/06/2020.
//  Copyright © 2020 Personal. All rights reserved.
//

import Foundation

struct ComentsService {
    
    let apiClient: APIClient
    
    static let shared = ComentsService()
    
    init(apiClient: APIClient = .shared ) {
        self.apiClient = apiClient
    }
    
    func getComments(postId: Int, pageNumber: Int = 0, pageLimit: Int = 20, sorting field: String = "date", sortBy order: Sorting = .descending, completionHandler: @escaping ([Comment]?, Error?) -> Void) {
        let url = Endpoint.comments(postId: postId, pageNumber: pageNumber, pageLimit: pageLimit, sorting: field, sortBy: order).url
        apiClient.get(url, completionHandler: completionHandler)
    }
    
}
