//
//  Posts.swift
//  MicroBlog
//
//  Created by Ravikumar Chintakayala on 28/06/2020.
//  Copyright © 2020 Personal. All rights reserved.
//

import Foundation

struct Post: Decodable {
    
    let authorId: Int
    
    let body:String
    
    let date: String
    
    let id: Int
    
    let imageUrl: String
    
    let title: String
    
    lazy var publishedDate : String? = {
        let text = date.getDate()?.string(format: "MMM dd, YYYY")
        return text != nil ? "Posted on \(text!)" : ""
    } ()
    
}
