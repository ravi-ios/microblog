//
//  BaseTableViewController.swift
//  MicroBlog
//
//  Created by Ravikumar Chintakayala on 28/06/2020.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

enum DataFetchType: Int {
    case loading
    case pagination
}

class BaseTableViewController: UITableViewController {
    
    // MARK: - Private members
    
    private let paginationIndicator = UIActivityIndicatorView(style: .medium)
    private let loadingIndicator = UIActivityIndicatorView(style: .large)
    
    // MARK: - Public members
    
    public var pageNumber = 1

    // MARK: - ViewController life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableHeaderView = loadingIndicator
        tableView.tableFooterView = paginationIndicator
    }
    
    // MARK: - Internal methods
    
    func showLoadingAnimation(_ type: DataFetchType) {
        switch type {
        case .loading:
            tableView.tableHeaderView?.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 50)
            loadingIndicator.startAnimating()
        case .pagination: paginationIndicator.startAnimating()
        }
    }
    
    func stopLoadingAnimation(_ type: DataFetchType) {
        switch type {
        case .loading:
            loadingIndicator.stopAnimating()
            tableView.tableHeaderView?.frame = .zero
        case .pagination: paginationIndicator.stopAnimating()
        }
    }
    
    func updateTableView(totalDataCount: Int, newDataCount: Int) {
        var indexPaths = [IndexPath]()
        for index in totalDataCount - newDataCount ..< totalDataCount {
            indexPaths.append(IndexPath(row: index, section: 0))
        }
        tableView.insertRows(at: indexPaths, with: .automatic)
    }
    
}
