//
//  AuthorsTableViewController.swift
//  MicroBlog
//
//  Created by Ravikumar Chintakayala on 28/06/2020.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

class AuthorsTableViewController: BaseTableViewController {
    
    // MARK: - Private members
    
    private var authors = [Author]()
    private let authorsService = AuthorsService()
    
    // MARK: - ViewController life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getAuthors(pageNumber: 1, fetchType: .loading)
    }
    
    // MARK: - Private methods
    
    private func getAuthors(pageNumber: Int, fetchType: DataFetchType = .loading) {
        showLoadingAnimation(fetchType)
        authorsService.getAuthors(pageNumber: pageNumber) { [weak self] result, error in
            guard let self = self else { return }
            
            defer { self.stopLoadingAnimation(fetchType) }
            
            guard error == nil else {
                NavigationService.shared.showAlert(message: error?.localizedDescription ?? "Sorry, some thing went wrong")
                return
            }
            
            guard let result = result else { return }
            
            self.authors.append(contentsOf: result)
            self.pageNumber += 1
            self.updateTableView(totalDataCount: self.authors.count, newDataCount: result.count)
        }
    }
    
}

// MARK: - Table view data source

extension AuthorsTableViewController {
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(AuthorTableViewCell.self)") as! AuthorTableViewCell
        cell.author = authors[indexPath.row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return authors.count
    }
    
}

// MARK: - Table view delegate

extension AuthorsTableViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NavigationService.shared.navigateToPosts(authors[indexPath.row])
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = 0
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && authors.count > 0 {
            getAuthors(pageNumber: pageNumber, fetchType: .pagination)
        }
    }
    
}
