//
//  PostsTableViewController.swift
//  MicroBlog
//
//  Created by Ravikumar Chintakayala on 28/06/2020.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

class PostsTableViewController: BaseTableViewController {
    
    // MARK: - Private properties
    
    private var posts = [Post]()
    private var headerView: AuthorHeaderView!
    
    // MARK: - Public properties
    
    public var author : Author!
    
    // MARK: - ViewController life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView = AuthorHeaderView.instantiate(author)
            
        getPosts(pageNumber: 1, fetchType: .loading)
    }
    
    // MARK: - Private properties
    
    private func getPosts(pageNumber: Int, fetchType: DataFetchType = .loading) {
        showLoadingAnimation(fetchType)
        PostsService.shared.getPosts(autherId: author.id, pageNumber: pageNumber) { [weak self] result, error in
            guard let self = self else { return }
            
            defer {
                self.stopLoadingAnimation(fetchType)
            }
            
            guard error == nil else {
                NavigationService.shared.showAlert(message: error?.localizedDescription ?? "Sorry, some thing went wrong")
                return
            }
            
            guard let result = result else { return }
                                    
            self.posts.append(contentsOf: result)
            self.pageNumber += 1
            self.updateTableView(totalDataCount: self.posts.count, newDataCount: result.count)

        }
    }
    
}

// MARK: - Table view data source

extension PostsTableViewController {
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(PostTableViewCell.self)") as! PostTableViewCell
        cell.post = posts[indexPath.row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }

}

// MARK: - Table view delegate

extension PostsTableViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NavigationService.shared.navigateToComments(posts[indexPath.row])
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = 0
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && posts.count > 0 {
            getPosts(pageNumber: pageNumber, fetchType: .pagination)
        }
    }
    
}
