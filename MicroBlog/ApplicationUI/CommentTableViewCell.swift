//
//  CommentsTableViewCell.swift
//  MicroBlog
//
//  Created by Ravikumar Chintakayala on 28/06/2020.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    // MARK: - TableViewCell life cycle
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        avatarImageView.image = UIImage(named: "Avatar")
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var commentedDateLabel: UILabel!
    
    // MARK: - Internal members
    
    var comment: Comment! {
        didSet {
            avatarImageView.imageWithPath(comment.avatarUrl)
            nameLabel.text = comment.userName
            commentLabel.text = comment.body
            commentedDateLabel.text = comment.commentedDate
        }
    }

}
