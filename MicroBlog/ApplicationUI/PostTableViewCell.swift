//
//  PostsTableViewCell.swift
//  MicroBlog
//
//  Created by Ravikumar Chintakayala on 28/06/2020.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {
    
    // MARK: - TableViewCell life cycle
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        postImageView.image = UIImage(named: "Default")
    }
    
    // MARK: - IBOutlets

    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var postedDateLabel: UILabel!

    // MARK: - Internal members
    
    var post: Post! {
        didSet {
            postImageView.imageWithPath(post.imageUrl)
            titleLabel.text = post.title
            descriptionLabel.text = post.body
            postedDateLabel.text = post.publishedDate
        }
    }

}
