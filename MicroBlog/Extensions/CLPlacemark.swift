//
//  CLPlacemark.swift
//  MicroBlog
//
//  Created by Ravikumar Chintakayala on 28/06/2020.
//  Copyright © 2020 Personal. All rights reserved.
//

import CoreLocation
import Foundation

extension CLPlacemark {

    var compactAddress: String? {
        guard var description = name else { return nil }
        
        if let city = locality { description += ", \(city)" }
        if let country = country { description += ", \(country)" }

        return description
    }

}
