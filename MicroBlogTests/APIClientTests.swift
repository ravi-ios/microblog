//
//  APIClientTests.swift
//  MicroBlogTests
//
//  Created by Ravikumar Chintakayala on 28/06/2020.
//  Copyright © 2020 Personal. All rights reserved.
//

@testable import MicroBlog
import XCTest

class APIClientTests: XCTestCase {
    
    private var apiClient: APIClient!

    override func setUpWithError() throws {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockURLProtocol.self]
        let urlSession = URLSession(configuration: configuration)
        
        apiClient = APIClient(urlSession: urlSession)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testSessionTaskFailure() throws {
        let url = URL(string: "https://test.com")
        let errorDescription = "Test error description"
        
        MockURLProtocol.response = nil
        MockURLProtocol.error = NSError(domain: "com.blogservice", code: 400, userInfo: [NSLocalizedDescriptionKey: errorDescription]) as Error
        
        let expectation = XCTestExpectation(description: "Error response")
        
        apiClient.get(url) { (result: Data?, error) in
            XCTAssertEqual(result, nil)
            XCTAssertEqual(error?.localizedDescription, errorDescription)
            
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 1)
    }

    func testSessionTaskResponse() throws {
        let url = URL(string: "https://test-response.com")
        
        let testData = "{\"id\": 1,\"name\": \"test-name\",\"userName\": \"test-user-name\",\"email\": \"test@yahoo.com\",\"avatarUrl\":\"test.jpeg\",\"address\": {\"latitude\": \"73.5451\",\"longitude\": \"155.4534\"}}"
        
        MockURLProtocol.response = Data(testData.utf8)
        MockURLProtocol.error = nil

        let expectation = XCTestExpectation(description: "response")
        
        apiClient.get(url) { (result: Author?, error) in

            XCTAssertEqual(error?.localizedDescription, nil)
            XCTAssertEqual(result?.id, 1)
            XCTAssertEqual(result?.name, "test-name")
            XCTAssertEqual(result?.userName, "test-user-name")
            XCTAssertEqual(result?.email, "test@yahoo.com")

            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1)
    }
    
    func testDownloadTaskFailure() throws {
        let url = URL(string: "https://test.png")
        let errorDescription = "Test error description"
        
        MockURLProtocol.response = nil
        MockURLProtocol.error = NSError(domain: "com.blogservice", code: 400, userInfo: [NSLocalizedDescriptionKey: errorDescription]) as Error
        
        let expectation = XCTestExpectation(description: "Error response")
    
        apiClient.download(url) { response, error  in
            XCTAssertEqual(response, nil)
            XCTAssertEqual(error?.localizedDescription, errorDescription)
            
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 1)
    }

    func testDownloadTaskResponse() throws {
        let url = URL(string: "https://test-response.png")
        let testData = "test-data"
        
        MockURLProtocol.response = Data(testData.utf8)
        MockURLProtocol.error = nil

        let expectation = XCTestExpectation(description: "response")

        apiClient.download(url) { response, error  in
            let responceString = String(decoding: response!, as: UTF8.self)

            XCTAssertEqual(error?.localizedDescription, nil)
            XCTAssertEqual(responceString, testData)

            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1)
    }
    
}
