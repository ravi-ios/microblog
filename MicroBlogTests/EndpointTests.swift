//
//  EndpointTests.swift
//  MicroBlogTests
//
//  Created by Ravikumar Chintakayala on 28/06/2020.
//  Copyright © 2020 Personal. All rights reserved.
//

@testable import MicroBlog
import XCTest

class EndpointTests: XCTestCase {
    
    let pageNumber = 10
    let pageLimit = 50

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testAuthorsEndpointUrl() throws {
        let expectedUrlString = "https://sym-json-server.herokuapp.com/authors?_page=\(pageNumber)&_limit=\(pageLimit)"
                
        let endpoint = Endpoint.authors(pageNumber: pageNumber, pageLimit: pageLimit)
        
        XCTAssertTrue(endpoint.url!.absoluteString == expectedUrlString, "Constructed url must be \(expectedUrlString)")
    }

}
